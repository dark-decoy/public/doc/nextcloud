* SSH Access
* Normal Business
* Weekend Use
* How much data needs to be transferred?
* How much free disk space?
* Any special configurations?
  * What Nextcloud apps are installed?
  * Nextcloud Theming?
* DNS Setup
  * Where is it hosted?
  * Get Access to?
  * Talk through what changes need to happen
    * Changes that need to happen prior to migration
    * Changes that need to happen the day of migration
* Migration Date
* Post Migration meeting
* Questions?